#include <iostream>
#include <fstream>
#include <vector>
#include <cuda_runtime_api.h>
#include <chrono>

#define CUDA_CHECK_ERROR(err)           \
if ((err) != cudaSuccess)               \
{          \
    printf("Cuda error: %s\n", cudaGetErrorString(err));    \
    printf("Error in file: %s, line: %i\n", __FILE__, __LINE__);  \
}

#define n 3 // длина одномерной цепочки
#define J 1 // обменный интегралл (для ферромагнетика 1, для аниферромагнетика -1)

int get_SP_cores(cudaDeviceProp devProp)
{
    int cores = 0;
    int mp = devProp.multiProcessorCount;
    switch (devProp.major){
        case 2: // Fermi
            if (devProp.minor == 1) cores = mp * 48;
            else cores = mp * 32;
            break;
        case 3: // Kepler
            cores = mp * 192;
            break;
        case 5: // Maxwell
            cores = mp * 128;
            break;
        case 6: // Pascal
            if ((devProp.minor == 1) || (devProp.minor == 2)) cores = mp * 128;
            else if (devProp.minor == 0) cores = mp * 64;
            else printf("Unknown device type\n");
            break;
        case 7: // Volta and Turing
            if ((devProp.minor == 0) || (devProp.minor == 5)) cores = mp * 64;
            else printf("Unknown device type\n");
            break;
        case 8: // Ampere
            if (devProp.minor == 0) cores = mp * 64;
            else if (devProp.minor == 6) cores = mp * 128;
            else printf("Unknown device type\n");
            break;
        default:
            printf("Unknown device type\n");
            break;
    }
    return cores;
}

struct Chain
{
    int8_t s[n]{};
};

struct Gem
{
    int32_t G=0;
    int16_t E=0;
    int16_t M=0;
    std::vector<uint32_t>conf;
};

struct Gem_part
{
    int16_t E=0;
    int16_t M=0;
    uint32_t conf=0;
    bool l = false;
};

void thread_init(std::vector<Chain>&chain, int length)
{
    for (auto i=0; i<length; i++)
    {
        int bit = i;
        for (auto & j : chain[i].s)
        {
            j = bit & 1 ? 1 : -1;
            bit>>=1;
        }
    }
}

void sort(std::vector<Gem_part>&gem_in, std::vector<Gem>&gem_out)
{
    for(int i=0; i<gem_in.size(); i++)
    {
        if(!gem_in[i].l) break;
        bool out = true;
        for(int j=0; j<gem_out.size(); j++)
        {
            out = false;
            if(gem_out[j].E == gem_in[i].E && gem_out[j].M == gem_in[i].M)
            {
                gem_out[j].G++;
                for(int k=0; k<gem_out[j].conf.size(); k++)
                {
                    if(gem_out[j].conf[k] == gem_in[i].conf) break;
                    if(k==gem_out[j].conf.size()-1)
                    {
                        gem_out[j].conf.resize(gem_out[j].conf.size()+1);
                        gem_out[j].conf[gem_out[j].conf.size()-1]=gem_in[i].conf;
                    }
                }
                break;
            }
            if(j==(gem_out.size()-1))
            {
                out = true;
            }
        }
        if(out)
        {
            gem_out.resize(gem_out.size() + 1);
            gem_out[gem_out.size()-1].G = 1;
            gem_out[gem_out.size()-1].E = gem_in[i].E;
            gem_out[gem_out.size()-1].M = gem_in[i].M;
            gem_out[gem_out.size()-1].conf.resize(1);
            gem_out[gem_out.size()-1].conf[0] = gem_in[i].conf;
        }
    }
}

void outputs(std::vector<Gem>&gem_o, std::vector<Gem>&gem_output)
{
    std::ofstream file_o("Gem_o.txt");
    std::ofstream file_output("Gem_output.txt");
    int G_sum = 0;
    if(n<4)
    {
        std::cout << "gem original:\n";
        for (auto &i : gem_o)
        {
            std::cout << "G=" << i.G
                      << " E=" << i.E
                      << " M=" << i.M
                      << " configurations:";
            for (unsigned int j : i.conf)
            {
                std::cout << " " << j;
            }
            std::cout << "\n";
        }
        std::cout << "\ngem odd:\n";
        for (auto &i : gem_output)
        {
            std::cout << "G=" << i.G
                      << " E=" << i.E
                      << " M=" << i.M
                      << " configurations:";
            for (unsigned int j : i.conf)
            {
                std::cout << " " << j;
            }
            std::cout << "\n";
        }
        for (auto &i : gem_o)
        {
            file_o << "G=" << i.G
                   << " E=" << i.E
                   << " M=" << i.M
                   << " configurations:";
            for (unsigned int j : i.conf)
            {
                file_o << " " << j;
            }
            file_o << "\n \n";
        }
        for (auto &i : gem_output)
        {
            G_sum += i.G;
            file_output << "G=" << i.G
                        << " E=" << i.E
                        << " M=" << i.M
                        << " configurations:";
            for (unsigned int j : i.conf)
            {
                file_output << " " << j;
            }
            file_output << "\n \n";
        }
        std::cout << "Sum of G = " << G_sum << "\n";
    }
    else
    {
        for (auto &i : gem_o)
        {
            file_o << "G=" << i.G
                   << " E=" << i.E
                   << " M=" << i.M
                   << " configurations:";
            for (unsigned int j : i.conf)
            {
                file_o << " " << j;
            }
            file_o << "\n \n";
        }
        for (auto &i : gem_output)
        {
            G_sum += i.G;
            file_output << "G=" << i.G
                        << " E=" << i.E
                        << " M=" << i.M
                        << " configurations:";
            for (unsigned int j : i.conf)
            {
                file_output << " " << j;
            }
            file_output << "\n \n";
        }
        file_output << "Sum of G = " << G_sum << "\n";
    }
}

__global__ void generator_gem_o_GPU(Chain *chain, Gem_part *gem_part, int part_size, int length, int number)
{
    auto x = threadIdx.x + blockIdx.x*blockDim.x;
    gem_part[x].l = false;
    if(x < (length - part_size*number))
    {
        gem_part[x].E = 0;
        gem_part[x].M = 0;
        for(auto i=0; i<n; i++)
        {
            gem_part[x].E += -J * chain[x].s[i] * chain[x].s[(i + 1) % n];
            gem_part[x].M += chain[x].s[i];
        }
        gem_part[x].conf = x+number*part_size;
        gem_part[x].l = true;
    }
}

__global__ void odd_GPU(Gem *gem_o, const int32_t *conf_o, Gem *gem_even, const int32_t *conf_even, Gem_part *gem_part, Chain *chain,
                        size_t cuda_threads, size_t conf_size, int i, int j, int k, int l)
{
    auto x = threadIdx.x + blockIdx.x*blockDim.x;
    gem_part[x+l*cuda_threads].l = false;
    if(x < (conf_size - cuda_threads*l))
    {
        gem_part[x+k*cuda_threads+l*cuda_threads].E = gem_even[i].E + gem_o[j].E;
        for (int m = 0; m < n; ++m)
        {
            gem_part[x+k*cuda_threads+l*cuda_threads].E += J * chain[conf_even[k]].s[m] * chain[conf_o[x+l*cuda_threads]].s[m];
        }
        gem_part[x+k*cuda_threads+l*cuda_threads].M = gem_even[i].M + gem_o[j].M;
        gem_part[x+k*cuda_threads+l*cuda_threads].conf = conf_o[x];
        gem_part[x+k*cuda_threads+l*cuda_threads].l = true;
    }
}

__global__ void even_GPU(Gem *gem_o, const int32_t *conf_o, Gem *gem_odd, const int32_t *conf_odd, Gem_part *gem_part, Chain *chain,
                         size_t cuda_threads, size_t conf_size, int i, int j, int k, int l)
{
    auto x = threadIdx.x + blockIdx.x*blockDim.x;
    gem_part[x+k*cuda_threads+l*cuda_threads].l = false;
    if(x < (conf_size - cuda_threads*l))
    {
        gem_part[x+k*cuda_threads+l*cuda_threads].E = gem_odd[i].E + gem_o[j].E;
        for (int m = 0; m < n; ++m)
        {
            gem_part[x+k*cuda_threads+l*cuda_threads].E += J * chain[conf_odd[k]].s[m] * chain[conf_o[x+l*cuda_threads]].s[m];
        }
        gem_part[x+k*cuda_threads+l*cuda_threads].M = gem_odd[i].M + gem_o[j].M;
        gem_part[x+k*cuda_threads+l*cuda_threads].conf = conf_o[x];
        gem_part[x+k*cuda_threads+l*cuda_threads].l = true;
    }
}

int main()
{
    auto t1 = std::chrono::high_resolution_clock::now();
    cudaDeviceProp dev{};
    cudaGetDeviceProperties(&dev, 0);
    int length = 1<<n;
    size_t cuda_threads = get_SP_cores(dev) * 256;
    std::cout << "Parallel threads = " << cuda_threads << "\n";
    int spread = length/cuda_threads+1;
    dim3 block_dim = 256;
    dim3 grid_dim = get_SP_cores(dev);
    std::vector<Chain>chain(length);
    std::vector<Gem_part>gem_part(cuda_threads);
    std::vector<Gem>gem_o;
    std::vector<Gem>gem_even;
    std::vector<Gem>gem_odd;
    int32_t *dev_conf_o, *dev_conf_even, *dev_conf_odd;
    Chain *dev_chain;
    Gem_part *dev_gem_part;
    Gem *dev_gem_o, *dev_gem_even, *dev_gem_odd;
    thread_init(chain, length);
    CUDA_CHECK_ERROR(cudaMalloc(&dev_gem_part, cuda_threads*sizeof(Gem_part)))
    CUDA_CHECK_ERROR(cudaMalloc(&dev_chain, chain.size()*sizeof(Chain)))
    CUDA_CHECK_ERROR(cudaMemcpy(dev_chain, chain.data(), chain.size()*sizeof(Chain), cudaMemcpyHostToDevice))
    for(int i=0; i<spread; ++i)
    {
        generator_gem_o_GPU<<<grid_dim, block_dim>>>(dev_chain, dev_gem_part, cuda_threads, length, i);
        CUDA_CHECK_ERROR(cudaMemcpy(gem_part.data(), dev_gem_part, gem_part.size()*sizeof(Gem_part), cudaMemcpyDeviceToHost))
        sort(gem_part, gem_o);
    }
    CUDA_CHECK_ERROR(cudaFree(dev_gem_part))
    gem_even = gem_o;
    CUDA_CHECK_ERROR(cudaMalloc(&dev_gem_o, gem_o.size()*sizeof(Gem)))
    CUDA_CHECK_ERROR(cudaMemcpy(dev_gem_o, gem_o.data(), gem_o.size()*sizeof(Gem), cudaMemcpyHostToDevice))
    for(int t=0; t<n; ++t)
    {
        if(!(t%2))
        {
            CUDA_CHECK_ERROR(cudaMalloc(&dev_gem_even, gem_even.size() * sizeof(Gem)))
            CUDA_CHECK_ERROR(cudaMemcpy(dev_gem_even, gem_even.data(), gem_even.size() * sizeof(Gem), cudaMemcpyHostToDevice))
            for(int i=0; i<gem_even.size(); ++i)
            {
                for(int j=0; j<gem_o.size(); ++j)
                {
                    size_t conf_even_size = gem_even[i].conf.size();
                    size_t part_size = cuda_threads*gem_even[i].G*gem_o[j].G;
                    CUDA_CHECK_ERROR(cudaMalloc(&dev_gem_part, part_size*sizeof(Gem_part)))
                    CUDA_CHECK_ERROR(cudaMalloc(&dev_conf_even, conf_even_size * sizeof(uint32_t)))
                    CUDA_CHECK_ERROR(cudaMemcpy(dev_conf_even, gem_even[i].conf.data(), conf_even_size * sizeof(uint32_t), cudaMemcpyHostToDevice))
                    for(int k=0; k<gem_even[i].conf.size(); ++k)
                    {
                        size_t conf_o_size = gem_o[j].conf.size();
                        CUDA_CHECK_ERROR(cudaMalloc(&dev_conf_o, conf_o_size * sizeof(uint32_t)))
                        CUDA_CHECK_ERROR(cudaMemcpy(dev_conf_o, gem_o[j].conf.data(), conf_o_size * sizeof(uint32_t), cudaMemcpyHostToDevice))
                        for(int l=0; l<(conf_o_size/cuda_threads + 1); ++l)
                        {
                            odd_GPU<<<grid_dim, block_dim>>>(dev_gem_o, dev_conf_o, dev_gem_even, dev_conf_even, dev_gem_part, dev_chain,
                                                             cuda_threads, conf_o_size, i, j, k, l);
                        }
                        CUDA_CHECK_ERROR(cudaFree(dev_conf_o))
                    }
                    gem_part.resize(part_size);
                    CUDA_CHECK_ERROR(cudaMemcpy(gem_part.data(), dev_gem_part, part_size * sizeof(Gem_part), cudaMemcpyDeviceToHost))
                    sort(gem_part, gem_odd);
                    CUDA_CHECK_ERROR(cudaFree(dev_gem_part))
                }
                CUDA_CHECK_ERROR(cudaFree(dev_conf_even))
            }
            CUDA_CHECK_ERROR(cudaFree(dev_gem_even))
        }
        if(t%2)
        {
            CUDA_CHECK_ERROR(cudaMalloc(&dev_gem_odd, gem_odd.size() * sizeof(Gem)))
            CUDA_CHECK_ERROR(cudaMemcpy(dev_gem_odd, gem_odd.data(), gem_odd.size() * sizeof(Gem), cudaMemcpyHostToDevice))
            for(int i=0; i<gem_odd.size(); ++i)
            {
                for(int j=0; j<gem_o.size(); ++j)
                {
                    size_t conf_odd_size = gem_odd[i].conf.size();
                    size_t part_size = cuda_threads*gem_odd[i].G*gem_o[j].G;
                    CUDA_CHECK_ERROR(cudaMalloc(&dev_gem_part, part_size*sizeof(Gem_part)))
                    CUDA_CHECK_ERROR(cudaMalloc(&dev_conf_odd, conf_odd_size * sizeof(uint32_t)))
                    CUDA_CHECK_ERROR(cudaMemcpy(dev_conf_odd, gem_odd[i].conf.data(), conf_odd_size * sizeof(uint32_t), cudaMemcpyHostToDevice))
                    for(int k=0; k<gem_odd[i].conf.size(); ++k)
                    {
                        size_t conf_o_size = gem_o[j].conf.size();
                        CUDA_CHECK_ERROR(cudaMalloc(&dev_conf_o, conf_o_size * sizeof(uint32_t)))
                        CUDA_CHECK_ERROR(cudaMemcpy(dev_conf_o, gem_o[j].conf.data(), conf_o_size * sizeof(uint32_t), cudaMemcpyHostToDevice))
                        for(int l=0; l<(conf_o_size/cuda_threads + 1); ++l)
                        {
                            even_GPU<<<grid_dim, block_dim>>>(dev_gem_o, dev_conf_o, dev_gem_odd, dev_conf_odd, dev_gem_part, dev_chain,
                                                             cuda_threads, conf_o_size, i, j, k, l);
                        }
                    CUDA_CHECK_ERROR(cudaFree(dev_conf_o))
                    }
                    gem_part.resize(part_size);
                    CUDA_CHECK_ERROR(cudaMemcpy(gem_part.data(), dev_gem_part, part_size * sizeof(Gem_part), cudaMemcpyDeviceToHost))
                    sort(gem_part, gem_even);
                    CUDA_CHECK_ERROR(cudaFree(dev_gem_part))
                }
                CUDA_CHECK_ERROR(cudaFree(dev_conf_odd))
            }
        CUDA_CHECK_ERROR(cudaFree(dev_gem_odd))
        }
    }
    CUDA_CHECK_ERROR(cudaFree(dev_chain))
    CUDA_CHECK_ERROR(cudaFree(dev_gem_o))
    if(!(n%2))
    {
        outputs(gem_o, gem_odd);
    }
    if(n%2)
    {
        outputs(gem_o, gem_even);
    }
    auto t2 = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
    std::cout << "working time is " << time/3600000 << " h " << (time%3600000)/60000 << " m "
              << ((time%3600000)%60000)/1000 << " s " << ((time%3600000)%60000)%1000 << " ms \n";
}